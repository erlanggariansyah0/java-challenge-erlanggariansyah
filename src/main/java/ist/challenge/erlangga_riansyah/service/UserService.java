package ist.challenge.erlangga_riansyah.service;

import ist.challenge.erlangga_riansyah.model.User;

import java.util.List;

public interface UserService {
    User register(User user) throws Exception;
    List<User> getListUser();
}
