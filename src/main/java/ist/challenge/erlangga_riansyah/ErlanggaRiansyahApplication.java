package ist.challenge.erlangga_riansyah;

import ist.challenge.erlangga_riansyah.model.User;
import ist.challenge.erlangga_riansyah.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class ErlanggaRiansyahApplication {
	public static void main(String[] args) {
		SpringApplication.run(ErlanggaRiansyahApplication.class, args);
	}

	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	CommandLineRunner commandLineRunner(UserRepository userRepository) {
		return args -> {
			User user = new User("anggi", passwordEncoder().encode("anggi"));

			userRepository.save(user);
		};
	}
}
