package ist.challenge.erlangga_riansyah.repository;

import ist.challenge.erlangga_riansyah.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
