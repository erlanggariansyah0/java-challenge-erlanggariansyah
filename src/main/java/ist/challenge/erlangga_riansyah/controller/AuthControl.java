package ist.challenge.erlangga_riansyah.controller;

import ist.challenge.erlangga_riansyah.model.User;
import ist.challenge.erlangga_riansyah.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1")
public class AuthControl {
    private final UserService userService;

    @GetMapping(value = "/users")
    public ResponseEntity<List<User>> getUserList() {
        return ResponseEntity.ok().body(userService.getListUser());
    }

    @PostMapping(value = "/register")
    public ResponseEntity<Object> register(
            @RequestParam String username,
            @RequestParam String password
    ) {
        User user = new User(username, password);
        User registeredUser;

        try {
            registeredUser = userService.register(user);
        } catch (Exception e) {
            Map<String, String> error = new HashMap<>();
            error.put("error", e.getMessage());

            return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
        }

        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/register").toUriString());
        return ResponseEntity.created(uri).body(registeredUser);
    }
}
